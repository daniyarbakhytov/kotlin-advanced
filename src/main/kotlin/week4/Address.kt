package week4

import kotlinx.serialization.Serializable
import kotlin.random.Random

@Serializable
data class Address(
    val city: String,
    val street: String,
    val houseNumber: String,
    val apartment: String
)

fun generateAddress() : Address {
    val city = generateCity()
    val street = generateStreet()
    val houseNumber = generateHouseNumber()
    val apartment = generateApartment()

    return Address(
        city = city,
        street = street,
        houseNumber = houseNumber,
        apartment = apartment
    )
}

fun generateCity(): String {
    val cities = listOf("Moskow", "Kazan", "Sochi")

    return cities.random()
}

fun generateStreet(): String {
    val streets = listOf("Lenina", "Gagarina", "Pushkina")

    return streets.random()
}

fun generateHouseNumber(): String {
    val houseNumber = Random.nextInt(1, 100)

    return houseNumber.toString()
}

fun generateApartment(): String {
    val apartmentNumber = Random.nextInt(1, 100)

    return apartmentNumber.toString()
}
