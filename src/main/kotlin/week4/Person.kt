package week4

import com.itextpdf.kernel.pdf.PdfDocument
import com.itextpdf.kernel.pdf.PdfWriter
import com.itextpdf.layout.Document
import com.itextpdf.layout.element.Table
import com.itextpdf.layout.properties.UnitValue
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.Json
import java.io.File
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import kotlin.random.Random


@Serializable
data class Person(
    val name: String,
    val surname: String,
    val patronymic: String,
    val gender: String,
    val age: Int,
    val dateOfBirth: String,
    val address: Address,
    val iin: String
)

fun generatePerson(): Person {
    val gender = generateGender()
    val age = generateAge()
    val address = generateAddress()

    return Person(
        name = generateName(gender),
        surname = generateSurname(gender),
        patronymic = generatePatronymic(gender),
        gender = gender,
        age = age,
        dateOfBirth = generateDateOfBirth(age),
        address = address,
        iin = generateINN()
    )
}

fun generateName(gender: String): String {
    val maleNames = listOf("Ivan", "Aleksandr", "Maksim")
    val femaleNames = listOf("Mariya", "Elena", "Anna")

    return if (gender == "m") maleNames.random() else femaleNames.random()
}

fun generateSurname(gender: String): String {
    val maleSurnames = listOf("Ivanov", "Petrov", "Sidorov")
    val femaleSurnames = listOf("Ivanova", "Petrova", "Sidorova")

    return if (gender == "f") maleSurnames.random() else femaleSurnames.random()
}

fun generatePatronymic(gender: String): String {
    val malePatronymics = listOf("Ivanovich", "Alekseyevich", "Maksimovich")
    val femalePatronymics = listOf("Ivanovna", "Alekseevna", "Maksimovna")

    return if (gender == "м") malePatronymics.random() else femalePatronymics.random()
}


fun generateGender(): String {
    return if (Random.nextBoolean()) "m" else "f"
}

fun generateAge(): Int {
    return Random.nextInt(16, 101)
}

fun generateDateOfBirth(age: Int): String {
    val currentYear = LocalDate.now().year
    val yearOfBirth = currentYear - age

    val isLeapYear = LocalDate.ofYearDay(yearOfBirth, 1).isLeapYear
    val lastDayOfYear = if (isLeapYear) 366 else 365

    val birthDate = LocalDate.ofYearDay(yearOfBirth, Random.nextInt(1, lastDayOfYear + 1))
    return birthDate.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"))
}

fun generateINN(): String {
    val inn = (1..10).map { Random.nextInt(0, 10) }.toMutableList()

    val n11 = calculateControlNumber(inn, listOf(7, 2, 4, 10, 3, 5, 9, 4, 6, 8))
    inn.add(n11)

    val n12 = calculateControlNumber(inn, listOf(3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8))
    inn.add(n12)

    return inn.joinToString("")
}

fun calculateControlNumber(digits: List<Int>, coefficients: List<Int>): Int {
    val sum = digits.zip(coefficients).sumOf { it.first * it.second }
    return sum % 11 % 10
}

object FileHandler {
    fun serializeToJson(people: List<Person>): String {
        return Json.encodeToString(ListSerializer(Person.serializer()), people)
    }

    fun writeToFile(fileName: String, content: String) {
        val file = File(fileName)
        File(fileName).writeText(content)
        println("Файл создан. Путь: ${file.absolutePath}")
    }

    fun createPdf(people: List<Person>) {
        val fileName = "persons.pdf"
        val pdf = PdfDocument(PdfWriter(fileName))
        val document = Document(pdf)
        val table: Table = Table(
            UnitValue
                .createPercentArray(floatArrayOf(1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f))
        ).useAllAvailableWidth()

        table.addCell("Name")
        table.addCell("Surname")
        table.addCell("Patronymic")
        table.addCell("Gender")
        table.addCell("Age")
        table.addCell("Date of birth")
        table.addCell("City")
        table.addCell("Street")
        table.addCell("House number")
        table.addCell("Apartment")
        table.addCell("IIN")

        for (person in people) {
            table.addCell(person.name)
            table.addCell(person.surname)
            table.addCell(person.patronymic)
            table.addCell(person.gender)
            table.addCell(person.age.toString())
            table.addCell(person.dateOfBirth)
            table.addCell(person.address.city)
            table.addCell(person.address.street)
            table.addCell(person.address.houseNumber)
            table.addCell(person.address.apartment)
            table.addCell(person.iin)
        }

        document.setFontSize(10f)
        document.add(table)
        document.close()
        println("Файл создан. Путь: ${File(fileName).absolutePath}")
    }
}
