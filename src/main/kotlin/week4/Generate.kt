package week4

import week4.FileHandler.createPdf

fun main() {
    print("Введите количество людей: ")
    val n = readlnOrNull()?.toIntOrNull()?.takeIf { it in 1..30 } ?: return

    val people = (1..n).map { generatePerson() }

    val jsonString = FileHandler.serializeToJson(people)
    FileHandler.writeToFile("persons.json", jsonString)

    createPdf(people)
}
