package week1

data class PersonData(
    val name: String,
    val surname: String,
    val patronymic: String? = null,
    val gender: String,
    val dateOfBirth: String,
    val age: Int,
    val inn: String? = null,
    val snils: String? = null
)

fun configure(
    name: String,
    surname: String,
    patronymic: String? = null,
    gender: String,
    dateOfBirth: String,
    age: Int,
    inn: String? = null,
    snils: String? = null
): PersonData {
    return PersonData(name, surname, patronymic, gender, dateOfBirth, age, inn, snils)
}

fun main() {
    val person = configure(
        name = "Van",
        surname = "Darkholm",
        patronymic = null,
        gender = "Male",
        dateOfBirth = "1980-01-04",
        age = 43,
        inn = null,
        snils = null
    )

    println(person.toString())
}
