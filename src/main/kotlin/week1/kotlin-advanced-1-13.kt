package week1

import java.time.LocalDate
import kotlin.math.round
import java.util.logging.Handler

fun typeCasting(obj: Any?) {
    when (obj) {
        is String -> println("Я получил String = $obj, ее длина равна ${obj.length}")
        is Int -> println("Я получил Int = $obj, его квадрат равен ${obj * obj}")
        is Double -> println("Я получил Double = $obj, это число округляется до ${round(obj * 100) / 100}")
        is LocalDate -> {
            val foundationDate = LocalDate.of(2006, 12, 24)
            val message = if (obj.isBefore(foundationDate)) "эта дата меньше чем дата основания Tinkoff" else "она больше даты основания Tinkoff"
            println("Я получил LocalDate = $obj, $message")
        }
        null -> println("Объект равен null")
        else -> println("Мне этот тип неизвестен(")
    }
}

fun main() {
    typeCasting("Privet")
    typeCasting(145)
    typeCasting(145.0)
    typeCasting(145.2817812)
    typeCasting(LocalDate.of(1990, 1, 1))
    typeCasting(null)
    typeCasting(Handler::class)
}
