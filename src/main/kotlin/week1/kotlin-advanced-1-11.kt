package week1

class MyClass {
    companion object {
        private var count = 0

        fun counter() {
            count++
            println("Вызван counter. Количество вызовов = $count")
        }
    }
}

fun main() {
    MyClass.counter()
    MyClass.counter()
    MyClass.counter()
    MyClass.counter()
}
