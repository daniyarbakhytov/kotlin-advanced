package week1

fun printArgs(vararg params: String) {
    println("Передано ${params.count()} элемента:")
    params.forEach { print("$it;") }
}

fun main() {
    printArgs("12","122","1234","fpo")
}
