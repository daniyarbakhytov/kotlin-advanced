package week1

fun configurePerson(
    name: String,
    surname: String,
    gender: String,
    dateOfBirth: String,
    age: Int,
    patronymic: String? = null,
    inn: String? = null,
    snils: String? = null
) {
    println("name = $name, surname = $surname, ${patronymic?.let { "patronymic = $it, " } ?: ""}gender = $gender, age = $age, dateOfBirth = $dateOfBirth${inn?.let { ", inn = $it" } ?: ""}${snils?.let { ", snils = $it" } ?: ""}")
}

fun main() {
    configurePerson(
        name = "Van",
        surname = "Darkholml",
        gender = "Male",
        dateOfBirth = "1980-01-04",
        age = 43
    )

    configurePerson(
        name = "Igor",
        surname = "Nikolaev",
        patronymic = "Vladimirovich",
        gender = "FiveReasonMan",
        dateOfBirth = "1960-01-30",
        age = 63,
        inn = "1234567890",
        snils = "111-111-111"
    )
}
