package week2

val sum = { a: Double, b: Double -> a + b }
val subtraction = { a: Double, b: Double -> a - b }
val multiplication = { a: Double, b: Double -> a * b }
val division = { a: Double, b: Double -> if (b != 0.0) a / b else throw ArithmeticException() }

fun main() {
    println(calculator(getCalculationMethod("+"), 2.0, 3.0))
    println(calculator(getCalculationMethod("-"), 2.0, 13.0))
    println(calculator(getCalculationMethod("*"), 2.0, 3.0))
    println(calculator(getCalculationMethod("/"), 2.0, 3.0))
}

fun calculator(lambda: ((Double, Double) -> Double), a: Double, b: Double): Double {
    return lambda(a, b)
}

fun getCalculationMethod(name: String): (Double, Double) -> Double {
    return when (name) {
        "+" -> sum
        "-" -> subtraction
        "*" -> multiplication
        "/" -> division
        else -> throw UnsupportedOperationException()
    }
}
