package week2

fun processNumbers(numbers: List<Double?>): Double {
    return numbers.filterNotNull()
        .map { if (it.toInt() % 2 == 0) it * it else it / 2 }
        .filter { it <= 25 }
        .sortedDescending()
        .take(10)
        .sum()
}

fun main() {
    val input1 = listOf(13.31, 3.98, 12.0, 2.99, 9.0)
    println("Искомая сумма элементов в оставшейся коллекции: ${"%.2f".format(processNumbers(input1))}")

    val input2 = listOf(133.21, null, 233.98, null, 26.99, 5.0, 7.0, 9.0)
    println("Искомая сумма элементов в оставшейся коллекции: ${"%.2f".format(processNumbers(input2))}")
}
