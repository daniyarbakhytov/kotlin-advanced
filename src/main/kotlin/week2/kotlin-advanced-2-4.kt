package week2

abstract class Pet

interface Runnable {
    fun run()
}

interface Swimmable {
    fun swim()
}

class Cat : Pet(), Runnable, Swimmable {
    override fun run() {
        println("Я кот и я бегаю")
    }

    override fun swim() {
        println("Я кот и я плаваю")
    }
}

open class Fish : Pet(), Swimmable {
    override fun swim() {
        println("Я рыба и я плаваю")
    }
}

open class Tiger : Pet(), Runnable, Swimmable {
    override fun run() {
        println("Я тигр и я бегаю")
    }

    override fun swim() {
        println("Я тигр и я плаваю")
    }
}

open class Lion : Pet(), Runnable {
    override fun run() {
        println("Я лев и я бегаю")
    }
}

open class Salmon : Fish() {
    override fun swim() {
        println("Я лосось и я плаваю")
    }
}

open class Tuna : Fish() {
    override fun swim() {
        println("Я тунец и я плаваю")
    }
}

fun <T : Runnable> useRunSkill(runner: T) {
    runner.run()
}

fun <T : Swimmable> useSwimSkill(swimmer: T) {
    swimmer.swim()
}

fun <T> useSwimAndRunSkill(pet: T) where T : Runnable, T : Swimmable {
    pet.run()
    pet.swim()
}

fun main() {
    val cat = Cat()
    val tiger = Tiger()
    val lion = Lion()
    val salmon = Salmon()
    val tuna = Tuna()

    useRunSkill(cat)
    useSwimSkill(cat)
    useSwimAndRunSkill(cat)

    useSwimAndRunSkill(tiger)
    useRunSkill(lion)
    useSwimSkill(salmon)
    useSwimSkill(tuna)
}
