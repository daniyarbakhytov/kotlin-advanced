package week2

val validateInn: (String) -> Boolean = validateInn@{ inn ->
    if (inn.length != 12) return@validateInn false

    val coefficients1 = listOf(7, 2, 4, 10, 3, 5, 9, 4, 6, 8)
    val coefficients2 = listOf(3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8)

    fun calculateControlNumber(coefficients: List<Int>, inn: String): Int {
        return coefficients.zip(inn.map { it.toString().toInt() })
            .sumOf { it.first * (it.second.toString().toInt()) } % 11 % 10
    }

    val controlNumber1 = calculateControlNumber(coefficients1, inn)
    val controlNumber2 = calculateControlNumber(coefficients2, inn)

    controlNumber1 == inn[10].toString().toInt() && controlNumber2 == inn[11].toString().toInt()
}

fun validateInnFunction(inn: String, function: (String) -> Boolean) {
    println("ИНН $inn ${if (function(inn)) "валиден" else "не валиден"}")
}

fun main() {
    val inn = "707574944998"
    validateInnFunction(inn, validateInn)
}
