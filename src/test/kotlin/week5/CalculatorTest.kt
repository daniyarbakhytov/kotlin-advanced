package week5

import org.junit.Assert.assertEquals
import org.junit.Assert.assertThrows
import org.junit.Assert.assertTrue
import org.junit.Test
import week2.calculator
import week2.getCalculationMethod

class CalculatorTest {

    @Test
    fun sumTest() {
        assertEquals(5.0, calculator(getCalculationMethod("+"), 2.0, 3.0), 0.001)
    }

    @Test
    fun subtractionTest() {
        assertTrue(calculator(getCalculationMethod("-"), 2.0, 13.0) == -11.0)
    }

    @Test
    fun multiplyTest() {
        assertEquals(896.0, calculator(getCalculationMethod("*"), 28.0, 32.0), 0.001)
    }

    @Test
    fun divisionTest() {
        assertEquals(8.2, calculator(getCalculationMethod("/"), 41.0, 5.0), 0.001)
    }

    @Test
    fun divisionByZeroTest() {
        assertThrows(ArithmeticException::class.java) { calculator(getCalculationMethod("/"), 1.0, 0.0) }
    }

    @Test
    fun unsupportedOperationTest() {
        assertThrows(UnsupportedOperationException::class.java) { calculator(getCalculationMethod(")"), 1.0, 0.0) }
    }
}
